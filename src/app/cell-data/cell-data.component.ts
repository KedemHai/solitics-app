import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-cell-data',
  templateUrl: './cell-data.component.html',
  styleUrls: ['./cell-data.component.scss'],
})
export class CellDataComponent implements OnInit {
  @Input() text?: string;

  constructor() { }

  ngOnInit(): void {
  }
}
