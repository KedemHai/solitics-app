import {Component, Input, OnInit} from '@angular/core';
import {ListType} from '../courses.service';

@Component({
  selector: 'app-list-type',
  templateUrl: './course-list-type.component.html',
  styleUrls: ['./course-list-type.component.scss'],
})
export class CourseListTypeComponent implements OnInit {
  @Input() listType?: ListType;

  constructor() { }

  ngOnInit(): void {
  }

  public getListTypeName(): string | undefined {
    if (this.listType === ListType.ALL) {
      return 'ראשית';
    }

    if (this.listType === ListType.DYNAMIC) {
      return 'דינמית';
    }
    if (this.listType === ListType.EMPTY) {
      return 'ריקה';
    }

    return;
  }

  public hasListType(): boolean {
    return (
        typeof this.listType !== 'undefined' &&
        this.listType !== ListType.NONE
    );
  }
}
