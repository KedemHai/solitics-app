import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  TemplateRef,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'app-date-format',
  templateUrl: './date-format.component.html',
  styleUrls: ['./date-format.component.scss'],
})
export class DateFormatComponent implements OnInit, OnChanges {
  @Input() date?: Date;
  @Input() format: 'DEFAULT' | 'FULL' = 'DEFAULT';
  public templateFormat: TemplateRef<any> | null = null;

  @ViewChild('formatDefault', {read: TemplateRef, static: true})
  private readonly formatDefault: TemplateRef<any> | null = null;

  @ViewChild('formatFull', {read: TemplateRef, static: true})
  private readonly formatFull: TemplateRef<any> | null = null;

  constructor() { }

  ngOnInit(): void {
    this.handleFormatTemplate();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('format')) {
      this.handleFormatTemplate();
    }
  }

  private handleFormatTemplate(): void {
    switch (this.format) {
      case 'DEFAULT':
        this.templateFormat = this.formatDefault;
        break;
      case 'FULL':
        this.templateFormat = this.formatFull;
        break;
    }
  }
}
