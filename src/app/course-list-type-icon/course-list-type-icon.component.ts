import {Component, Input, OnInit} from '@angular/core';
import {ListType} from '../courses.service';

@Component({
  selector: 'app-list-type-icon',
  templateUrl: './course-list-type-icon.component.html',
  styleUrls: ['./course-list-type-icon.component.scss'],
})
export class CourseListTypeIconComponent implements OnInit {
  @Input() listType?: ListType;

  public get isAll(): boolean {
    return this.listType === ListType.ALL;
  }

  public get isDynamic(): boolean {
    return this.listType === ListType.DYNAMIC;
  }

  public get isEmpty(): boolean {
    return this.listType === ListType.EMPTY;
  }

  constructor() { }

  ngOnInit(): void {
  }
}
