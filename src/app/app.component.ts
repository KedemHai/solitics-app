import {Component, OnInit} from '@angular/core';
import {CourseData, CoursesService} from './courses.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  private readonly coursesService: CoursesService;
  public courses$?: Observable<CourseData[]>;

  public constructor(coursesService: CoursesService) {
    this.coursesService = coursesService;
  }

  ngOnInit(): void {
    this.courses$ = this.coursesService.getCourses();
  }
}
