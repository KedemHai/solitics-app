import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {CellDataComponent} from './cell-data/cell-data.component';
import {CourseListTypeComponent} from './course-list-type/course-list-type.component';
import {CourseAttendeesComponent} from './course-attendees/course-attendees.component';
import {CourseRowComponent} from './course-row/course-row.component';
import {CourseListTypeIconComponent} from './course-list-type-icon/course-list-type-icon.component';
import {HttpClientModule} from '@angular/common/http';
import {DateFormatComponent} from './date-format/date-format.component';

@NgModule({
  declarations: [
    AppComponent,
    CellDataComponent,
    CourseListTypeComponent,
    CourseAttendeesComponent,
    CourseRowComponent,
    CourseListTypeIconComponent,
    DateFormatComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
