enum CourseAttendeesType {
  ACTIVE = 'ACTIVE',
  NEW = 'NEW',
  REMOVED = 'REMOVED',
  BLOCKED = 'BLOCKED',
  STOPPED = 'STOPPED',
  TOTAL = 'TOTAL',
}

export default CourseAttendeesType;
