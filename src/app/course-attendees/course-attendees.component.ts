import {Component, HostBinding, Input, OnInit} from '@angular/core';
import CourseAttendeesType from './CourseAttendeesType';

@Component({
  selector: 'app-listings-data',
  templateUrl: './course-attendees.component.html',
  styleUrls: ['./course-attendees.component.scss'],
})
export class CourseAttendeesComponent implements OnInit {
  @Input() count?: number;
  @Input() dataType?: CourseAttendeesType | string;

  @HostBinding('attr.data-listing-type')
  public get attrDataListingType(): string {
    switch (this.dataType) {
      case CourseAttendeesType.ACTIVE:
        return 'active';
      case CourseAttendeesType.REMOVED:
        return 'removed';
      case CourseAttendeesType.NEW:
        return 'new';
      default:
        return 'default';
    }
  }

  constructor() { }

  ngOnInit(): void {
  }

  public listingDataText(): string {
    if (this.dataType === CourseAttendeesType.ACTIVE) {
      return 'פעילים';
    }
    if (this.dataType === CourseAttendeesType.BLOCKED) {
      return 'חסומים';
    }
    if (this.dataType === CourseAttendeesType.NEW) {
      return 'נוספו היום';
    }
    if (this.dataType === CourseAttendeesType.REMOVED) {
      return 'הוסרו';
    }
    if (this.dataType === CourseAttendeesType.STOPPED) {
      return 'הופסקו';
    }
    if (this.dataType === CourseAttendeesType.TOTAL) {
      return 'סה"כ';
    }
    return '';
  }
}
