import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

interface CourseDataResponse {
  course: {
    name: string,
    date: string,
  },
  isDynamic?: boolean,
  isAllList?: boolean,
  state: {
    total: number,
    active?: number,
    new?: number,
    removed?: number,
    blocked?: number,
    stopped?: number,
  }
}

export enum ListType {
  ALL,
  DYNAMIC,
  EMPTY,
  NONE,
}

export interface CourseData {
  name: string,
  date: Date,
  listType: ListType,
  total?: number,
  active?: number,
  new?: number,
  removed?: number,
  blocked?: number,
  stopped?: number,
}

@Injectable({
  providedIn: 'root',
})
export class CoursesService {
  private readonly httpClient: HttpClient;

  public constructor(httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  public getCourses(): Observable<CourseData[]> {
    return this.httpClient.get<CourseDataResponse[]>('assets/courses.json').
        pipe(
            map((response) => {
              return response.map<CourseData>((data) => {

                function convertListType(data: CourseDataResponse): ListType {
                  if (data.isDynamic) {
                    return ListType.DYNAMIC;
                  }
                  if (data.isAllList) {
                    return ListType.ALL;
                  }
                  if (data.state.total === 0) {
                    return ListType.EMPTY;
                  }

                  return ListType.NONE;
                }

                return {
                  name: data.course.name,
                  date: new Date(data.course.date),
                  listType: convertListType(data),
                  active: data.state.active,
                  blocked: data.state.blocked,
                  new: data.state.new,
                  removed: data.state.removed,
                  stopped: data.state.stopped,
                  total: data.state.total,
                };
              }).sort((a) => {
                if (a.listType === ListType.ALL) {
                  return -1;
                }

                return 0;
              });
            }),
        );
  }
}
