import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {CourseData, ListType} from '../courses.service';

@Component({
  selector: 'app-course-row',
  templateUrl: './course-row.component.html',
  styleUrls: ['./course-row.component.scss'],
})
export class CourseRowComponent implements OnInit {
  @Input() courseData?: CourseData;

  public get isEmpty(): boolean {
    return this.courseData?.listType === ListType.EMPTY;
  }

  @HostBinding('class.is-all-list')
  public get isAllList(): boolean {
    return this.courseData?.listType === ListType.ALL;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
